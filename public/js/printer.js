console.log('running from front-end')

class talker {
    constructor(id, data){
        this.id = id;
        this.data = data
    }
    sayhi(data) {
        this.data = data
        return `${this.data.saludo}`
    }

    saySomethingRandom(data){
        this.data = data
        console.log(this.data)
        return this.data
    }
}

let mi_data ={
    saludo : 'Hola',
    dialog:{
        inconvexed : ['A','B','C'],
        time : 10
    }
}
let ridnois = new talker('Ridnois', mi_data)
let entrance = document.getElementsByClassName('talker')[0]
entrance.innerHTML = `<h1>${ridnois.sayhi()}</h1>`
