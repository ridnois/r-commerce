# R-Commerce - Mercado de recursos

## requerimientos del sistema 

NodeJS 8.11.x o superior
Git 1.6 o superior
Npm 5.10.0 o superior
MongoDB 3.x.x o superior (opcional)
Visual Studio code o tu editor de codigo favorito :)

## como empezar 

Una vez hayas instalado las dependencias necesarias, debes clonar el repositorio

git clone r-commerce https://gitlab.com/ridnois/r-commerce.git

Luego, debes instalar las dependencias de npm

npm install

Correr servidor local

cd functions

node server.js



## Acerca del Proyecto
 
    INFORMATION IMPORTANTE: Codigo fuente en proceso de desarrollo. Bienvenidos son los aportes y las sugerencias
                           Toda forma de colaboracion sera enormemente agradecida. 
               
    Para saber mas Contactar directamente: Sebastian Altamirano
  
    Correo Electronico : s.altamiranorid@gmail.com
    Gitlab : https://gitlab.com/ridnois
    Facebook : Sebastian altamirano
    Proximanente sitio Web : https://ridnois.com
 
### Areas en las que puedes ayudar

    Diseño de interfaz y estetica de la aplicacion
    Modelado de base de datos
    Testeo
    Modelado del codigo fuente
    Desarrollo en plataformas 

### Objetivo del proyecto   
   Desarrollar un nuevo modelo economico, y como vivimos en el siglo XXI lo haremos en codigo. para explicar la idea a
   mayor cabalidad es de enorme utilidad que usted conozca los fundamentos de la nuestra economia basada en debito (si usted desea instruirse respecto al tema, le recomiendo "Money modern mechanics" ).

### Arbol de recursos
Los recursos poseen una estructura arborea, donde los recursos padre van generando a su vez productos hijos

### Conexiones

Todos los productos estan conectados, ya que siempre poseeran recursos en comun.

Crear un objeto en un mercado basado en recursos es crear una rama dentro del arbol

arbol de recursos
