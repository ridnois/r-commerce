'use strict'
/***
 * 
function primos(n){
    var primo;
    primo = true
    for(let i = 2; i< n; i++){
        if(n % i == 0){
            primo = false
            break;
        }
    }
    return `${n} es primo : ${primo}`
}

for(let i = 0; i < 10; i++){
    console.log(primos(i))
}
*/
function palindromos(frase){
    let clean_frase = frase.replace(/\s+/g,'')
    let reversed = clean_frase.split("").reverse().join().replace(/,/g,'')
    if(typeof frase == 'string'){
        if(clean_frase.toLowerCase() == reversed.toLowerCase()){
            console.log(`${frase} - es palindromo`)
        }
    }
}
palindromos('Anita lava la tina')