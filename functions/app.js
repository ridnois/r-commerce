'use strict'


const express = require('express');
const app = express();
const pug = require('pug');
const router = require('./router');
const bodyParser = require('body-parser');
const cookieParser= require('cookie-parser');
const mongoose = require('mongoose');
const morgan = require('morgan')
const jwt = require('jsonwebtoken');
const config = require('./config');//config file
const user = require('./models/user');

let port = process.env.PORT || 8080;

//mongoose.connect(config.database)
app.set('superSecret', config.secret)
app.use(morgan('dev'))

//espress settings 
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cookieParser())
app.set('views', './views')
app.set('view engine','pug')
app.use(express.static('../public'))
app.use('/', router)
app.use(express.json())
//info
console.log('Working')

//app.listen(3000)

module.exports = app
