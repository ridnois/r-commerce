'use strict'

let moongose = require('moongose');

let UserSchema = new moongose.Schema({
    email: {
        type: String,
        unique: true,
        required: true,
        trim: true
    },
    username : {
        type: String,
        unique: true,
        required: true,
        trim: true
    },
    password: {
        type: String,
        required: true,
    },
    passwordConf: {
        type: String,
        required: true,
    }
})

let User = moongose.model('User', UserSchema)

module.exports = User