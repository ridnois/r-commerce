'use stric'
let http =require('http')
let algorithm = require('lib/algorithm.js')

class DataRes{
    constructor(id){
        this.id = id
        this.type = 'application/json'
        this.dataFormat = 'text/plain'
        this.port = 3000
        this.data = {
            "formated" : "Api running at " + this.port 
        }
    }
}

let mi_server = new DataRes('Api')

http.createServer((req, res)=> {
    res.writeHead(200, {'Content-Type' : mi_server.dataFormat})
    res.end(mi_server.data.formated)
    console.log(`Server running at ${DataRes.port}`)
})

.listen(mi_server.port)