//Construye la funcionalidad total antes de extender las clases
'use strict'
class Resource{
    constructor(id){
        this.id = id
        this.distribution = []
    }
    distribute(productor, mount){
        let consumer = [productor.id, mount]
        return this.distribution.length === 0 ? this.add(consumer) : this.check(consumer)
    }
    add(consumer){
        this.distribution.push(consumer)
    } 
    check(consumer){
        let q = null
        for(var i = 0; i < this.distribution.length; i++){
            if(this.distribution[i][0] === consumer[0]){
                q = true
            }
            else{
                q = false
            }
        }
        return q ? this.update(consumer) : this.add(consumer)
    }
    update(consumer){
        for(var i = 0; i< this.distribution.length; i++){
            if(this.distribution[i][0]=== consumer[0]){
                this.distribution[i][1] += consumer[1]
            }
        }
    }
}
class Productor{
    constructor(id){
        this.id = id
        this.items=[]
    }
    get(resource, mount){
        let purchase = [resource.id, mount]
        if(this.items.length === 0){
            this.add(purchase)
        }
        else{
            this.check(purchase)
        }
        return resource.distribute(this, mount)
    }
    add(purchase){
        this.items.push(purchase)
    }
    check(purchase){
        //Leeremos el arreglo de items, si existe en la wallet se modifica, de otra forma se añade :) Que sencillo era despues de todo jaja, me estuve craneando dos horas :(
        let exists_on_wallet = null
        for(var i = 0; i < this.items.length; i++){
            if( this.items[i][0]=== purchase[0]){exists_on_wallet = true }
            else{ exists_on_wallet = false }
        }
        return exists_on_wallet ? this.update(purchase) : this.add(purchase)
    }
    update(purchase){
        //esta era mi confusion, recordaba que necesitaba leer el arerglo dos veces pero no sabia por que
        for(var i = 0; i < this.items.length; i++){
            if(this.items[i][0]=== purchase[0]){
                this.items[i][1] += purchase[1]
            }
        }
    }
}


//ES5
module.exports = {Resource, Productor}




