/** ___________________________________________________________________________________________________________
 * |___________________________________________________________________________________________________________|
 * |_____________000000000_______0000____0000000_________0000___0000______00000000______0000_______0000________|
 * |____________000000000_______0000____000000000_______0000___0000____000000000000____0000_____00000000_______|
 * |___________0000____0000____0000____0000___ 0000____00000__0000____0000____0000____0000____0000--0000_______|
 * |__________0000____0000____0000____0000___ 0000____00000000000____0000____0000____0000____0000______________|
 * |_________000000000_______0000____0000___ 0000____00000000000____0000____0000____0000_____0000______________|
 * |________000000000_______0000____0000___ 0000____0000__00000____0000____0000____0000_______000000___________|
 * |_______0000____0000____0000____0000___ 0000____0000___0000____0000____0000____0000__________0000___________|
 * |______0000____0000____0000____0000___ 0000____0000___0000____0000____0000____0000____0000__0000____________|
 * |_____0000____0000____0000____000000000_______0000___0000____000000000000____0000_____00000000______________|
 * |____0000____0000____0000____00000000________0000___0000______00000000______0000_______0000_________________|
 * |___________________________________________________________________________________________________________|
 * |___________________The_Amazing_Resource_Store (just kidding, just a bike store  ;) )_______________________|
 * |________________________________________________2018_______________________________________________________|
 */
 let logo = `/** ___________________________________________________________________________________________________________
 * |___________________________________________________________________________________________________________|
 * |_____________000000000_______0000____0000000_________0000___0000______00000000______0000_______0000________|
 * |____________000000000_______0000____000000000_______0000___0000____000000000000____0000_____00000000_______|
 * |___________0000____0000____0000____0000___ 0000____00000__0000____0000____0000____0000____0000--0000_______|
 * |__________0000____0000____0000____0000___ 0000____00000000000____0000____0000____0000____0000______________|
 * |_________000000000_______0000____0000___ 0000____00000000000____0000____0000____0000_____0000______________|
 * |________000000000_______0000____0000___ 0000____0000__00000____0000____0000____0000_______000000___________|
 * |_______0000____0000____0000____0000___ 0000____0000___0000____0000____0000____0000__________0000___________|
 * |______0000____0000____0000____0000___ 0000____0000___0000____0000____0000____0000____0000__0000____________|
 * |_____0000____0000____0000____000000000_______0000___0000____000000000000____0000_____00000000______________|
 * |____0000____0000____0000____00000000________0000___0000______00000000______0000_______0000_________________|
 * |___________________________________________________________________________________________________________|
 * |___________________________________The_Amazing_Resource_Store______________________________________________|
 * |________________________________________________2018_______________________________________________________|`
/**
 *  INFORMATION IMPORTANTE: Codigo fuente en proceso de desarrollo. Bienvenidos son los aportes y las sugerencias
 *                          Toda forma de colaboracion sera enormemente agradecida. 
 *              
 *  Para saber mas Contactar directamente: Sebastian Altamirano
 * 
 *  Correo Electronico : s.altamiranorid@gmail.com
 *  Gitlab : https://gitlab.com/ridnois
 *  Facebook : Sebastian altamirano
 *  Proximanente sitio Web : https://ridnois.com
 */

 /**
  * Ridnois - 2018 Licencia de codigo abierto
  */
 
 /**
  * REQUERIMIENTOS DEL SISTEMA: NodeJS 8.11.2 o superior
  *                             MongoDB 3.0.0 o superior 
  * 
  */

 /**
  * Objetivo del codigo
  * 
  * 
  * ECONOMIA ORIENTADA A OBJETOS
  *  
  * 
  * Desarrollar un nuevo modelo economico, y como vivimos en el siglo XXI lo haremos en codigo. para explicar mi idea a
  * mayor cabalidad es de enorme utilidad que usted conozca los fundamentos de la nuestra economia basada en debito (si usted desea instruirse respecto al tema, le recomiendo "Money modern mechanics" ).
  * 
  * Asi como todos los individuos confian en el dinero como elemento de cambio, podria darsele esa confianza a los objetos mismos, con un poco de ayuda de la  tecnologia moderna podria automatizarse
  * la forma en que los productos van adquiriendo su valor. Desde hace decadas se ha hecho de esta forma, traduciendo especies en monedas, apostando al alza y la baja recursos que por justicia mas
  * aya de la humana, nos pertenece a todos.
  * 
  * El mercado actual fomenta la deuda, Las monedas se devaluan, pierden su poder adquisitivo lentamente mientras el valor de los productos es estatico (Solo aparentemente)
  * 
  * Podriase, en vez de crear una deuda monetaria, estar en constante deuda con los recursos. y asi presionar a los productores a optimizar sus procesos. podriamos incluso hacer que a mayor velocidad
  * de flujo de los recursos, mayor recompensa para los productores.
  * 
  * Toda la forma en que conocemos el mundo puede revertirse, su perversion y su codicia pueden reemplazarse por una meritocracia impulsada por el amor y el reconocimiento.
  * 
  * 
  * He sido intencionalmente ambiguo, ¿lo que nos concierne a nosotros es el codigo verdad?
  */


  'use strict'
/**
 *  Objeto recurso: Producto, materia prima o servicio implementable, transferible y distribuible  
 */
class Resource {
    constructor(id){
        this.id = id
        this.global = null
        this.taken = null
        this.distribution = []
    }
    distribute(receiver, mount){
        let solicitud = [receiver, mount]
        return this.distribution.length === 0 ? this.add(solicitud) : this.check(solicitud)
    }
    add(consumer){
        this.distribution.push([consumer[0].id, consumer[1]])
        consumer[0].wallet.push([this.id, consumer[1]])
        this.taken += consumer[1]
    }
    check(consumer){
        let exists = null
        for(let client in this.distribution){
            if(this.distribution[client][0] === consumer[0].id){
                exists = true
            }
            else {
                exists = false
            }
        }
        return exists ? this.update(consumer) : this.add(consumer)
    }
    update(consumer){
        for(let id in this.distribution){
            if(this.distribution[id][0] === consumer[0].id){
                this.distribution[id][1] += consumer[1]
                consumer[0].get(this.id, consumer[1])
                this.taken += consumer[1]
            }
        }
    }
}
/**
 * El objeto recurso no es suficiente por si solo, debe interactual con el exchanger
 */
class Exchanger {
    constructor(id){
        this.id = id
        this.wallet = []
    }
    exchange(resource, mount){
        resource.distribute(this, mount)
    }
    get(id, mount){
        let exists = null
        for(let item in this.wallet){
            if(this.wallet[item][0] === id){
                exists = true
            }
            else{
                exists = false
            }
        }
        return exists ? this.update(id, mount) : this.add(id, mount)
    }
    update(id, mount){
        for(let item in this.wallet){
            if(this.wallet[item][0] === id){
                this.wallet[item][1] += mount
            }
        }
    }
    add(id, mount){
        this.wallet.push([id, mount])
    }
}
//let char = '|'
//clean_logo = logo.replace(/\x2a/g, '').replace(/\//g,'').replace(/_/g, ' ').replace(/\|/g,'').replace(/-/g, ' ')
//console.log(clean_logo)
// administrar tension dentro de los objetos 
class Product extends Resource
// A diferencia de un recurso, la vida util de un producto es limitada

{
    constructor(id){
        super(id)
        this.id = id
        this.components = null
    }
    //The more data you give, the more valuable is the item
    show(param){
        if(param in this){
            console.log('this exists')
        }
        else {
            console.log(`this doesn't exists`)
        }
        return typeof this[param] != 'undefined' ? this[param] : `Unable to proceed, this property doesn't exists`
    }
    // this functions analize if components exists as individual products
    settings(){
        //Target: esquematizar arbol de componentes para posteriormente evaluar que piezas son conocidas y cuales no
        this.settings = {
            composition : [],
            childrenComponents : []
        }
        for(let component in this.components){
            //console.log(typeof this.components[component])
            //console.log(typeof this.components[component])
            if(typeof this.components[component] == 'string' | typeof this.components[component] == 'object'){
                if(typeof this.components[component] == 'object'){
                    for(let subpiece in this.components[component]){
                        this.settings.childrenComponents.push(this.components[component][subpiece])
                    }
                }
                else {
                    this.settings.childrenComponents.push([component ,this.components[component]])
                }
            }
        }
        return this.settings
    }
}
let amazingBike = new Product('Specialized Dirt jumper')
let amazingHub = new Product('Novatec')
amazingBike.components = 
//the real modelation requires ids
{
    "wheels": {
        "rim" : "AlexRims 26",
        "hub" : "Novatec amazing hub",
        "spokes": "Random" 
    },
    "transmision": {
        "rear-derailleur": "Sram Eagle XX1 11v",
        "cog": "Sram Eagle XX1 11v 10t-43t",
        "front-derailleur": "none"
    },
    "frame": "Specialized p3",
    "brakes" : {
        "disks": "Shimano Saint MB8000",
        "calipers": "Shimano Saint"
    }
}
/*
let water = new Resource('Mineral water', 1)
let rider = new Exchanger('Ridnois')
rider.exchange(amazingBike, 1)
rider.exchange(water, 1)
console.log(rider)
console.log(water)
*/

//exportar

module.esports = {
    Resource, Exchanger
}

