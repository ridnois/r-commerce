'use strict'
var express = require('express'),
    router = express.Router(),
    bodyParser = require('body-parser'),
    dbController = require('./db/db_controller'),
    //siteController = require('./controllers/site-controller')
    jwt = require('jsonwebtoken')


router
    //development mode
    .get('/dev', (req, res) => {
        let developSite = {
            title : 'Ridnois develop',
            content :{
                message: 'Ridnois se encuentra bajo desarrollo, puedes unirte al proceso de desarollo',
                argument: 'Github'
            }
        }
        siteController.dev(req, res, developSite)
    })
    .get('/', (req, res) => {
        res.render('index', {title: 'Ridnois', message:'Ridnois'})
    })
    .get('/auth', (req, res, next) => {
        res.render('auth', {
            title : 'Registrate - Ridnois',
            message : 'Registrate'
        })
    })

    .post('/login', (req, res) => {
        let data = {
            email : req.body.email,
            pass : req.body.pass,
            pass2 : req.body.pass2
        }
        jwt.sign({data}, 'secret', {expiresIn: '30s'}, (err, token) => {
            if(err){
                console.log(err)
            }
            else{
                res.json({
                    data,
                    token
                })
            }
        })
     
    })
    .post('/pannel', verifyToken, (req, res) => {  
        jwt.verify(req.token, 'secret', (err, authData) => {
          if(err) {
            res.sendStatus(403);
          } else {
            res.json({
              message: 'Post created...',
              authData
            });
          }
        });
      });

      function verifyToken(req, res, next) {
        // Get auth header value
        const bearerHeader = req.headers['authorization'];
        // Check if bearer is undefined
        if(typeof bearerHeader !== 'undefined') {
          // Split at the space
          const bearer = bearerHeader.split(' ');
          // Get token from array
          const bearerToken = bearer[1];
          // Set the token
          req.token = bearerToken;
          // Next middleware
          next();
        } else {
          // Forbidden
          res.sendStatus(403)
        }
      
      }


module.exports = router